// the setup routine runs once when you press reset:
void setup() {                
  // initialize the digital pin as an output.
  pinMode(0, OUTPUT); //LED on Model B
  
  digitalWrite(0,HIGH);
  
  pinMode(1, OUTPUT);
  digitalWrite(1,LOW);

  pinMode(2, INPUT);
  digitalWrite(2,HIGH);

  pinMode(3, INPUT);
  digitalWrite(3,HIGH);
  // pin 4 is unusable for unknown reasons
  // pin 5 is unusable because it is a reset pin (to change that behaviour there's a fuse to flip, requires extra hardware and sofware)
      

}

//We have two usable buttons (N°3 and N°4 on the keypad)
int pin2 = 0;
int pin3 = 0;

int prev_pin2 = 0;
int prev_pin3 = 0;

int brightness = 254;
int led_on = 1;


// the loop routine runs over and over again forever:
void loop() {
  delay(10); // 20Hz refresh rate
  //button is pushed when pin is pulled down so consider inverted reads
  pin2 = !digitalRead(2);
  pin3 = !digitalRead(3);
  
  if (pin2 && !prev_pin2) {
    //button2 was pressed
    led_on = !led_on;
    prev_pin2 = 1;}
    
  if (!pin2)
    {prev_pin2 = 0;}

  if (pin3 && !prev_pin3) {
    //button3 was pressed
    prev_pin3 = 1;
    if (brightness <= 1){
      brightness = 254;
    }
    else{
      brightness = brightness >> 1;
    }
  if (!pin3)
    {prev_pin3 = 0;}

  if (led_on){
    analogWrite(0,brightness);}
  else{
    analogWrite(0,0);}
    
  }


  
}
    
  

  

