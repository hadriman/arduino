#include <Audio.h>
#include <Wire.h>
#include <SPI.h>
#include <SD.h>
#include <SerialFlash.h>

// GUItool: begin automatically generated code
AudioInputI2Sslave       i2sslave1;      //xy=208,263
AudioInputUSB            usb1;           //xy=213,171
AudioOutputI2Sslave      i2sslave2;      //xy=580,166
AudioOutputUSB           usb2;           //xy=597,248
AudioConnection          patchCord1(i2sslave1, 0, usb2, 0);
AudioConnection          patchCord2(i2sslave1, 1, usb2, 1);
AudioConnection          patchCord3(usb1, 0, i2sslave2, 0);
AudioConnection          patchCord4(usb1, 1, i2sslave2, 1);
// GUItool: end automatically generated code

int led = 13;
void setup() {                
  AudioMemory(12);
  pinMode(led, OUTPUT);
}

void loop() {
  digitalWrite(led, HIGH);   // turn the LED on (HIGH is the voltage level)
  delay(10);
  digitalWrite(led, LOW);
  delay(100);   
  digitalWrite(led, HIGH);   // turn the LED on (HIGH is the voltage level)
  delay(10);       // wait for a second
  digitalWrite(led, LOW);    // turn the LED off by making the voltage LOW
  delay(5000);    
}

// Teensyduino 1.35 & earlier had a problem with USB audio on Macintosh
// computers.  For more info and a workaround:
// https://forum.pjrc.com/threads/34855-Distorted-audio-when-using-USB-input-on-Teensy-3-1?p=110392&viewfull=1#post110392

