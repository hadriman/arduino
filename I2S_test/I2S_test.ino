#include <i2s.h>
#include <math.h>


int led = 5;

void setup() {
  
  //start i2s output
  i2s_begin();

  
  // setup i2s output
  uint32_t rate = 44100 ; //audio sample rate in kHz
  i2s_set_rate(rate);

  pinMode(led, OUTPUT);
}



void loop() {
  /*
  int i=0;
  double angle_step = 0.01;
  double amplitude_modulation = 0.000001;
  uint16_t sample;

  while (true){
    i++;
    sample = sin(angle_step*i)*sin(amplitude_modulation*i);
    i2s_write_lr(sample, sample);
  }
  */
  
  uint16_t left_sample = 42;
  uint16_t right_sample = 102;
  
  digitalWrite(led, HIGH);   

  for (int i = 2; i < 100; i++) {
    i2s_write_lr(left_sample, right_sample);
  }
  
  digitalWrite(led, LOW);
}
