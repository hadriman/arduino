#include <Audio.h>
#include <Wire.h>
#include <SPI.h>
#include <SD.h>
#include <SerialFlash.h>

// GUItool: begin automatically generated code
AudioSynthKarplusStrong  string2;        //xy=208,303
AudioInputUSB            usb1;           //xy=209,245
AudioInputI2Sslave       i2sslave2;      //xy=209,370
AudioSynthKarplusStrong  string1;        //xy=211,185
AudioMixer4              mixer1;         //xy=387,207
AudioMixer4              mixer2;         //xy=390,284
AudioOutputI2Sslave      i2sslave1;      //xy=547,248
AudioOutputUSB           usb2;           //xy=575,383
AudioConnection          patchCord1(string2, 0, mixer2, 3);
AudioConnection          patchCord2(usb1, 0, mixer1, 3);
AudioConnection          patchCord3(usb1, 1, mixer2, 0);
AudioConnection          patchCord4(i2sslave2, 0, usb2, 0);
AudioConnection          patchCord5(i2sslave2, 1, usb2, 1);
AudioConnection          patchCord6(string1, 0, mixer1, 0);
AudioConnection          patchCord7(mixer1, 0, i2sslave1, 0);
AudioConnection          patchCord8(mixer2, 0, i2sslave1, 1);
// GUItool: end automatically generated code




int ledState = LOW;  
long previousMillis = 0; 
long millisInterval = 1000;
float velocity = 0.5;
int freq = 2000;

void setup() {
  // put your setup code here, to run once:
  AudioMemory(150);
}

void loop() {
  unsigned long currentMillis = millis();
 
  if(currentMillis - previousMillis > millisInterval) {
    // save the last time you blinked the LED 
    previousMillis = currentMillis;   
    if (ledState == LOW){
      ledState = HIGH;
      string1.noteOn(freq, velocity);
      string2.noteOn(freq*2, velocity);
    }
    else{
      ledState = LOW;
      string1.noteOff(velocity);
      string2.noteOff(velocity);
    }
 
  }
 
}

