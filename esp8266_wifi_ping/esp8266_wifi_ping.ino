#include <ESP8266WiFi.h>
#include <ESP8266Ping.h>

extern "C" {
#include <user_interface.h>
}


const char* ssid     = "wifi";         // The SSID (name) of the Wi-Fi network you want to connect to
const char* pass = "pass";     // The password of the Wi-Fi network

//int status = WL_IDLE_STATUS; // the Wifi radio's status

void WIFI() {

//WiFi.disconnect();
//wifi_set_macaddr(STATION_IF, &mac[0]);
//WiFi.mode(WIFI_STA);
//wifi_set_macaddr(STATION_IF, &mac[0]);
delay(10);

Serial.println(WiFi.macAddress());


int j = 1;

// attempt to connect to Wifi network:
while (WiFi.status() != WL_CONNECTED && j<3) {
Serial.print("Attempting to connect to WPA SSID: ");
Serial.print(ssid);
// Connect to WPA/WPA2 network:
WiFi.begin(ssid,pass);
int i = 0;
while (WiFi.status() != WL_CONNECTED && i<10) {
Serial.print(".");
delay(6000);
i++;
}
j++;
}
Serial.println(".");

// you're connected now, so print out the data:
if (WiFi.status() == WL_CONNECTED){
Serial.print(F("You're connected to the network: "));
Serial.println(ssid);
Serial.print("IP Address: ");
Serial.println(WiFi.localIP());
}
else{
Serial.print(F("It hasn't been possible to connected to the network: "));
Serial.println(ssid);
}
}

void setup() {


Serial.begin(9600);
delay(10);

Serial.println(WiFi.macAddress());
wifi_set_macaddr(0, const_cast<uint8*>(mac));

Serial.println(WiFi.macAddress());
WIFI();

for (int i=0;i<5;i++){

  bool ret = Ping.ping("www.google.com");
  Serial.println(ret);
  }
}

void loop() {
// put your main code here, to run repeatedly:

}
