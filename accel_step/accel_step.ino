
// Bounce.pde
// -*- mode: C++ -*-
//
// Make a single stepper bounce from one limit to another
//
// Copyright (C) 2012 Mike McCauley
// $Id: Random.pde,v 1.1 2011/01/05 01:51:01 mikem Exp mikem $
#include <AccelStepper.h>
// Define a stepper and the pins it will use
//AccelStepper stepper; // Defaults to AccelStepper::FULL4WIRE (4 pins) on 2, 3, 4, 5
AccelStepper stepper(AccelStepper::FULL4WIRE, A0, A1, A2, A3);
void setup()
{  
  Serial.begin(9600);
  // Change these to suit your stepper if you want
  stepper.setMaxSpeed(10000);
  stepper.setAcceleration(15000);

}

void cycle(){
   stepper.moveTo(3);
    while (stepper.distanceToGo() != 0){stepper.run();}
    stepper.moveTo(-stepper.currentPosition());
     while (stepper.distanceToGo() != 0){stepper.run();}
      }
    
void loop()
{

 unsigned long t1 = millis();
 for(int i = 0;i<20;i++){
 //cycle();
 }
 Serial.print("Hz : ");
 Serial.println(1000*20/(float)(millis()-t1));
 
 delay(1000);
  
    
  
}
