#include <ESP8266WiFi.h>
#include <ESP8266Ping.h>

#include <WiFiUdp.h>
#include <ArduinoOTA.h>

#include <AccelStepper.h>
#include <Ticker.h>  //Ticker Library


extern "C" {
#include <user_interface.h>
}

//stepper 
AccelStepper stepper(AccelStepper::HALF4WIRE, D0, D2, D1, D3);
Ticker stepper_runner;

//WiFi
const char* ssid     = "wifi";         // The SSID (name) of the Wi-Fi network you want to connect to
const char* pass = "pass";     // The password of the Wi-Fi network


int status = WL_IDLE_STATUS; // the Wifi radio's status
WiFiServer wifiServer(8080);

//other
const short int LED = 2; //onboard led 

int stepperEnable = 0; //status of the stepper,
// defines whether it should be powered or not 

void WIFI()
{
  //WiFi.disconnect();
  WiFi.mode(WIFI_STA);
  delay(10);
  int j = 1;
  // attempt to connect to Wifi network:
  while (WiFi.status() != WL_CONNECTED && j<3) {
    Serial.print("Attempting to connect to WPA SSID: ");
    Serial.print(ssid);
    // Connect to WPA/WPA2 network:
    WiFi.begin(ssid,pass);
    int i = 0;
    while (WiFi.status() != WL_CONNECTED && i<10) {
      Serial.print(".");
      delay(6000);
      i++;
      }
    j++;
    }

Serial.println(".");

// you're connected now, so print out the data:
if (WiFi.status() == WL_CONNECTED){
Serial.print(F("You're connected to the network: "));
Serial.println(ssid);
Serial.print("IP Address: ");
Serial.println(WiFi.localIP());
}
else{
Serial.print(F("It hasn't been possible to connecte to the network: "));
Serial.println(ssid);
}
}


void blink(int n_blink) 
{
    for (int i=0; i<n_blink; i++){
        digitalWrite(LED,LOW);
        delay(300);
        digitalWrite(LED,HIGH);
        delay(300);

    }
}

void test_cycle(){ 
    stepperEnable = 1;
    stepper.enableOutputs();
    
    stepper.moveTo(30);
    while (stepper.distanceToGo() != 0){stepper.run();delay(1);}
    stepper.moveTo(0);
    while (stepper.distanceToGo() != 0){stepper.run();delay(1);}
    
    stepperEnable = 0;
    stepper.disableOutputs();
}

void run_stepper(){
    //interupt function responsible for moving the stepper
    // it should be called as often as possible


    if (stepper.distanceToGo() != 0){
        //digitalWrite(stepper_en,HIGH); //power ON stepper
        if (stepperEnable == 0){
            stepperEnable = 1;
            stepper.enableOutputs();
            digitalWrite(LED, 0); 
        }
        stepper.run();
    }

    else {
        if (stepperEnable == 1){
            stepperEnable = 0;
            stepper.disableOutputs();
            digitalWrite(LED, HIGH); // led is inverted
        }
        //digitalWrite(stepper_en,LOW); //power OFF stepper
    }

}

void setup() {
    pinMode(LED, OUTPUT);
    digitalWrite(LED,HIGH);
    Serial.begin(9600);
    WIFI();
    wifiServer.begin();


    ArduinoOTA.setHostname("esp8266_servo_laser_L065");
    ArduinoOTA.begin();
    
    stepper.setMaxSpeed(500); 
    stepper.setAcceleration(1000);


    test_cycle();
    blink(3);

    stepper_runner.attach_ms(1, run_stepper);
    //stepper_runner.attach_ms(100, run_stepper);



  //bool ret = Ping.ping("www.google.com");
  //Serial.println(ret);
  
  
  Serial.println("Ready");

}

void loop() {
    ArduinoOTA.handle();

    WiFiClient client = wifiServer.available();
    //Serial.println("wifi server available");

    if (client) {
        //Serial.println("client");

        while (client.connected()) {
            //Serial.println("client connected");

            while (client.available()>0) {
                Serial.println("client available");
                //int pos = client.read();
                String req = client.readStringUntil('\r');
                Serial.println(req);
                int pos = req.toInt();

                client.write("ACK");

                //int pos = client.read();
                if (stepperEnable == 0){
                    stepperEnable = 1;
                    stepper.enableOutputs();
                    delay(10);
                }
                Serial.println(pos);
                stepper.moveTo(pos);


                
                //Serial.write(pos);
             }

            delay(10);
        }

        client.stop();
        Serial.println("Client disconnected");

    }

}
