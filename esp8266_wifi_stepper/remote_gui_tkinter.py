#!/usr/bin/env python3

from time import sleep
from tkinter import *
from tkinter import ttk

import socket
host = "host.ip"
port = 8080

stp2nm = 0.1 #nm/stp
nm0 = 780


class RemoteStepper():
    def __init__(self,master,host=None,port=None,dummy=False):
        self.abs_pos = 0
        self.lmbda0 = nm0
        self.lmbda_pos = self.abs_pos*stp2nm + self.lmbda0

        # window layout 
        ## first row, entry
        frame_info = Frame(master, borderwidth=2, relief=GROOVE)
        frame_info.pack()

        self.abs_pos_label = Label(frame_info, text = f"pos:")
        self.abs_pos_label.pack(side=LEFT)

        self.abs_pos_entry=Entry(frame_info, width=10, )
        self.abs_pos_entry.insert(0, f"{self.abs_pos} stp")
        self.abs_pos_entry.pack(side=LEFT)
        self.abs_pos_entry.bind("<Return>", self.update_abs_pos_entry)

        self.lmbda_pos_entry=Entry(frame_info, width=10)
        self.lmbda_pos_entry.insert(0, f"{self.lmbda_pos:.2f} nm")
        self.lmbda_pos_entry.pack(side=LEFT)
        self.lmbda_pos_entry.bind("<Return>", self.update_lmbda_pos_entry)

        self.lmbda0_label = Label(frame_info, text = "λ₀ =")
        self.lmbda0_label.pack(side=LEFT,padx=5)

        self.lmbda0_entry=Entry(frame_info, width=10)
        self.lmbda0_entry.insert(0, f"{self.lmbda0:.2f} nm")
        self.lmbda0_entry.pack(side=LEFT)
        self.lmbda0_entry.bind("<Return>", self.update_lmbda0_entry)


        ## second row , slider
        frame_slider = Frame(master, borderwidth=2, relief=GROOVE)
        frame_slider.pack()

        self.slider_start = -1000
        self.slider_start_entry=Entry(frame_slider, width=10)
        self.slider_start_entry.insert(0, f"{self.slider_start}")
        self.slider_start_entry.pack(side=LEFT)
        self.slider_start_entry.bind("<Return>", self.update_slider_start_entry)

        self.slider_end = 1000
        self.slider_end_entry=Entry(frame_slider, width=10)
        self.slider_end_entry.insert(0, f"{self.slider_end}")
        self.slider_end_entry.bind("<Return>", self.update_slider_end_entry)
        
        self.disable_slider_callback = False
        self.slider_value = DoubleVar()
        self.slider = Scale(frame_slider, 
                variable = self.slider_value,
                orient='horizontal',
                resolution=1, tickinterval=200,
                length=500,
                from_=self.slider_start,
                to=self.slider_end,
                command=self.update_slider,
                label='Absolute position (step)' )
        self.slider.pack(side=LEFT)

        self.slider_end_entry.pack(side=LEFT)

        ## third row, move buttons
        frame_move = Frame(master, borderwidth=2, relief=GROOVE)
        frame_move.pack()
        self.button_move = []
        for value in ["-1000","-100","-10","-1","+1","+10","+100","+1000"]:
            self.button_move.append(
                    ttk.Button(frame_move,
                        text=value,
                        #borderwidth=1,
                        command = lambda v=int(value) : self.send(pos=v,relative=True)
                         )              
                    )
        for i,button in enumerate(self.button_move):
            button.grid(row=0, column=i)


        


        #start connection
        
        if dummy is False:
            self.s = self.connect(host,port)

        else :
            self.send = self.dummy_send 

    def connect(self,host,port):
        s = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
        s.connect((host,port))
        return s

    def send(self,pos,relative=False):
        #stepper is a 5wire unipolar with halfstepping
        if relative == True:
            self.abs_pos += int(pos)
        else : 
            self.abs_pos = int(pos)

        #message format is an absolute step position followed by a carriage return
        message = f"{self.abs_pos}\r"

        #print(message)
        self.s.sendall(message.encode("ascii"))
        response = self.s.recv(3)
        assert response == b"ACK"
        self.update_pos()

    def dummy_send(self,pos,relative=False):
        if relative == True:
            self.abs_pos += int(pos)
        else : 
            self.abs_pos = int(pos)
        print(f"sending {self.abs_pos}")
        sleep(0.1)
        self.update_pos()

    def update_abs_pos_entry(self, event=None):
        if event == None:
            self.abs_pos_entry.delete(0,END)
            self.abs_pos_entry.insert(0, f"{self.abs_pos} stp")

        else:
            abs_pos_rawinput = self.abs_pos_entry.get()
            abs_pos_parsed = int(re.sub('[^.\-\d]', '', abs_pos_rawinput))
            self.abs_pos = abs_pos_parsed
            self.send(self.abs_pos)

    def update_lmbda_pos_entry(self, event=None):
        if event == None:
            self.lmbda_pos = self.abs_pos*stp2nm + self.lmbda0
            self.lmbda_pos_entry.delete(0,END)
            self.lmbda_pos_entry.insert(0, f"{self.lmbda_pos:.2f} nm")

        else :
            lmbda_pos_rawinput = self.lmbda_pos_entry.get()
            lmbda_pos_parsed = float(re.sub('[^.\-\d]', '', lmbda_pos_rawinput))
            self.lmbda_pos = lmbda_pos_parsed
            self.abs_pos = int((self.lmbda_pos - self.lmbda0)/stp2nm)  
            #print(event,self.lmbda_pos_entry.get())
            self.send(self.abs_pos)
    
    def update_lmbda0_entry(self,event=None):
        if event == None:
            pass

        else :
            lmbda0_rawinput = self.lmbda0_entry.get()
            lmbda0_parsed = float(re.sub('[^.\-\d]', '', lmbda0_rawinput))
            self.lmbda0 = lmbda0_parsed

            self.update_lmbda_pos_entry()

    def update_slider_start_entry(self,event=None):
        if event == None:
            pass

        else :
            slider_start_rawinput = self.slider_start_entry.get()
            slider_start_parsed = int(re.sub('[^.\-\d]', '', slider_start_rawinput))
            self.slider_start = slider_start_parsed
            self.slider.configure(from_=self.slider_start)

    def update_slider_end_entry(self,event=None):
        if event == None:
            pass

        else :
            slider_end_rawinput = self.slider_end_entry.get()
            slider_end_parsed = int(re.sub('[^.\-\d]', '', slider_end_rawinput))
            self.slider_end = slider_end_parsed
            self.slider.configure(to=self.slider_end)

    def update_slider(self,value=None):
        #print(f"update slider {value}")
        if value == None:
            self.slider_value.set(self.abs_pos)

        else :
            #slider automatic update behaves the same as user input
            #so we only send data if it's new
            if self.abs_pos != int(value):
                self.abs_pos = int(value)
                self.send(self.abs_pos)


    def update_pos(self,event=None):
        self.update_abs_pos_entry(event=None)
        self.update_lmbda_pos_entry(event=None)
        self.update_slider(value=None)



    




root = Tk()
root.title("Stepper remote control")
remote = RemoteStepper(root, host, port)
#remote = RemoteStepper(root, host, port, dummy=True)
root.mainloop()

