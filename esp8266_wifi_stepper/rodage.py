#!/usr/bin/env python3

from time import sleep
import socket

host = "host.ip"
port = 8080


s = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
s.connect((host,port))

def send(pos):
    #message format is an absolute step position followed by a carriage return
    message = f"{pos}\r"

    print(message)
    s.sendall(message.encode("ascii"))
    response = s.recv(1024)
    #assert response == b"ACK"


send(0)
sleep(10)

for i in range(100):
    send(3000)
    sleep(10)
    send(0)
    sleep(10)
