#include <SPI.h>
#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>
#include <EEPROM.h>

#include <AccelStepper.h>
//pin connected to the stepper:  A0 to A3
AccelStepper stepper(AccelStepper::FULL4WIRE, A0, A1, A2, A3);

#define OLED_RESET 4
Adafruit_SSD1306 display(OLED_RESET);

#define NUMFLAKES 10
#define XPOS 0
#define YPOS 1
#define DELTAY 2




#if (SSD1306_LCDHEIGHT != 32)
#error("Height incorrect, please fix Adafruit_SSD1306.h!");
#endif

//buttons pins
int b1 = 6; //menu
int b2 = 4; //-
int b3 = 5; //+
int b4 = 3; //select

//UI state
int cur_menu = 1;
int cur_menu_addr = 0;

int n_menus = 4; 


//device state;
long int frequency = 10000; //mHz
int frequency_addr = 1;
int duty_cycle = 50; //in percent of time opened
int duty_cycle_addr = 2;
int shutter_cur_state = 0; //0 -> open , 1-> closed 
int shutter_cur_state_addr = 3;
int external_delay = 0;
int external_delay_addr = 4;
int shutter_stopped = 0;
int shutter_stopped_addr = 5;
int closed_pos = 7;
int closed_pos_addr = 6;
int spin_speed = 1000;
int spin_speed_addr = 7;


unsigned long time_out = millis();

void setup()   {
   //max speed is irrelevant for the shutter motion
   stepper.setMaxSpeed(10000); 

   //this defines the cycle amplitude  
   int closed_position = 7;
   stepper.moveTo(closed_position);
   while (stepper.distanceToGo() != 0){stepper.run();}
   shutter_cur_state = 1; 
   //for a 7 step cycle : 
   // frequency = sqrt( accel*0.022018577017912253)
   // accel  = (frequency**2) * 45.416195569154794
   stepper.setAcceleration(2000);
   Serial.begin(115200);
   pinMode(b1, INPUT_PULLUP);
   pinMode(b2, INPUT_PULLUP);
   pinMode(b3, INPUT_PULLUP);
   pinMode(b4, INPUT_PULLUP);
   //read_EEPROM_state();
   display.begin(SSD1306_SWITCHCAPVCC, 0x3C);  // initialize with the I2C addr 0x3C (for the 128x32)

  // Clear the buffer.
  display.clearDisplay();

  
  //setup interrupts
   
  cli();//stop interrupts
/*
  //set timer0 interrupt at 1kHz
  TCCR0A = 0;// set entire TCCR2A register to 0
  TCCR0B = 0;// same for TCCR2B
  TCNT0  = 0;//initialize counter value to 0
  // set compare match register for 1khz increments
  OCR0A = 240;// = (16*10^6) / (10000*64) - 1 (must be <256)
  // turn on CTC mode
  TCCR0A |= (1 << WGM01);
  // Set CS01 and CS00 bits for 64 prescaler
  TCCR0B |= (1 << CS01) | (1 << CS00);   
  // enable timer compare interrupt
  TIMSK0 |= (1 << OCIE0A);
 */
  //set timer1 interrupt at 10kHz
  TCCR1A = 0;// set entire TCCR1A register to 0
  TCCR1B = 0;// same for TCCR1B
  TCNT1  = 0;//initialize counter value to 0
  // set compare match register for 30hz increments
  OCR1A = 24;// = (16*10^6) / (30*64) - 1 (must be <65536)
  // turn on CTC mode
  TCCR1B |= (1 << WGM12);
  // Set CS12 and CS10 bits for 64 prescaler
  TCCR1B |= (1 << CS11) | (1 << CS10);  
  // enable timer compare interrupt
  TIMSK1 |= (1 << OCIE1A);

  sei();//allow interrupts

  
 

  }

void loop(){
  display.clearDisplay();
  refresh();  
  display.display();

  }


// Iterrupts functions


ISR(TIMER1_COMPA_vect){//timer1 interrupt 30Hz to refresh the screen

  if (cur_menu == 4){
    stepper.runSpeed();
    }
    
  else if (cur_menu != 1) // menu 1 is "stopped" mode
  {
    if(stepper.distanceToGo() != 0){
      stepper.run();
      }
    else{
      //flip shutter position
      if (shutter_cur_state == 0){
        stepper.moveTo(0);}
      else if (shutter_cur_state == 1){
        stepper.moveTo(closed_pos);}
         
      shutter_cur_state = !shutter_cur_state;
      stepper.run();
      }
   }
}
   


void read_EEPROM_state(void){
  //UI state
  cur_menu = EEPROM.read(cur_menu_addr);
  frequency = EEPROM.read(frequency_addr);
}

void write_EEPROM_state(void){
  EEPROM.write(cur_menu_addr, cur_menu);
  EEPROM.write(frequency_addr, frequency);
}


int cur_button_pressed;
void refresh(void){
  cur_button_pressed = buton_press();

  Serial.println(cur_button_pressed);
  
  if (cur_button_pressed == 1) {
         cur_menu ++;
         if (cur_menu>4)
            { cur_menu%=4;}
   }
  draw_menu_select();

  switch(cur_menu) {
      case 1 :
         menu_1();
         break; 
      case 2 :
         menu_2();
         break; 
      case 3 :
         menu_3();
         break;
      case 4 :
         menu_4();
         break;
      
   } 
}


int buton_press(void){
  //note : input_pullups are ON
  if (time_out + 300 < millis()){
  if (!digitalRead(b1)){
    time_out = millis();
    return 1;}
  else if (!digitalRead(b2)){
    time_out = millis();
    return 2;}
  else if (!digitalRead(b3)){
    time_out = millis();
    return 3;}
  else if (!digitalRead(b4)){  
    time_out = millis();
    return 4;}
  }
  return 0;
  }


void draw_menu_select(void){
  display.setTextSize(1);
  display.setTextColor(WHITE);
  display.setCursor(0,0);
  for(int i=1; i<=n_menus;i++){
    if (i==cur_menu){
        display.setTextColor(BLACK, WHITE); // 'inverted' text
        display.print(i); 
        display.setTextColor(WHITE);
      }
    else{
        display.print(i); 
      } 
  }
  display.println();
  }
 
void menu_1(void){
  
  
  display.setTextSize(1);
  display.setTextColor(WHITE);
  display.setCursor(50,0);
 
  display.print("stopped");
  
  display.setTextSize(2);
  display.setCursor(0,15);
  
  switch (shutter_cur_state){
    case 0:
      display.println("OPEN ");
      break;
    case 1:
      display.println("CLOSED ");
      break;
  }
  
  if (cur_button_pressed == 2 or cur_button_pressed == 3)
  {
    flip();
  }

  if (cur_button_pressed == 4) 
  {
    shutter_cur_state = 1 ;
  }
  
  

  }


long int decade_val = 10000;
int decade = 0;

void menu_2(void){
 display.setTextSize(1);
  display.setTextColor(WHITE);
  display.setCursor(50,0);


  
  display.print("frequency ");

  
  if (cur_button_pressed == 3) 
  {
      frequency = frequency +  decade_val;
      update_accel();
   }

  if (cur_button_pressed == 2) 
  {
    frequency = frequency - decade_val;
    update_accel();
  }
  
  if (frequency > 99999) 
    { frequency = 10;}
  
  if (frequency < 10) 
    { frequency = 99999;}
  
  if (cur_button_pressed == 4) 
  {
    decade++;
    if (decade >3)
      {decade = 0;}
    
    decade_val = 10000;
    for(int i=0;i<decade;i++){
      decade_val =  decade_val/10;
      }
    
  }
  
  display.setTextSize(2);
  display.setCursor(0,15);
 
  char buf[4];
  sprintf(buf, "%04ld", frequency/10);
  
  for (int i=0;i<4;i++){
    if (i==2){
      display.print(".");
    }
    if (i==(decade)) {
      display.setTextColor(BLACK, WHITE); // 'inverted' text
      display.print(buf[i]);
      display.setTextColor(WHITE);
    }
    else {
      display.print(buf[i]);
    }
    
   }
  display.println(" Hz");
  
  }


void menu_3(void){
  display.setTextSize(1);
  display.setTextColor(WHITE);
  display.setCursor(50,0);
 
  display.print("amplitude ");

  if (cur_button_pressed == 3) 
  {
    closed_pos++;
  }

  if (cur_button_pressed == 2) 
  {
    closed_pos--;
  }
  
  if (cur_button_pressed == 4) 
  {
    closed_pos = 7;
  }

  display.setTextSize(2);
  display.setCursor(0,15);
  display.print(closed_pos);
  display.println(" steps");
  
 }


  
int speed_decade_val = 1000;
int speed_decade = 0;

  
void menu_4(void){
  
  display.setTextSize(1);
  display.setTextColor(WHITE);
  display.setCursor(50,0);
  display.print("spin ");

  
  if (cur_button_pressed == 3) 
  {
      spin_speed = spin_speed +  speed_decade_val;
   }

  if (cur_button_pressed == 2) 
  {
    spin_speed = spin_speed -  speed_decade_val;
  }
  
  if (cur_button_pressed == 4) 
  {
    speed_decade++;
    if (speed_decade >4)
      {speed_decade = 0;}
    
    speed_decade_val = 10000;
    for(int i=0;i<speed_decade;i++){
      speed_decade_val =  speed_decade_val/10;
      }  
  }
  stepper.setSpeed(spin_speed);
  
  display.setTextSize(2);
  display.setCursor(0,15);
 
  char buf[5];
  sprintf(buf, "%05d", spin_speed);
  
  for (int i=0;i<5;i++){
    if (i==(speed_decade)) {
      display.setTextColor(BLACK, WHITE); // 'inverted' text
      display.print(buf[i]);
      display.setTextColor(WHITE);
    }
    else {
      display.print(buf[i]);
    }
    
   }
  display.println("stp/s");
  
  }
  
void menu_5(void){
  display.setTextSize(1);
  display.setTextColor(WHITE);
  display.setCursor(50,0);
 
  display.print("duty cycle ");

  if (cur_button_pressed == 3) 
  {
    duty_cycle++;
  }

  if (cur_button_pressed == 2) 
  {
    duty_cycle--;
  }
  
  if (cur_button_pressed == 4) 
  {
    duty_cycle = 50;
  }

  display.setTextSize(2);
  display.setCursor(0,15);
  display.print(duty_cycle);
  display.println("% open");
  
  
  }

  
  
void menu_6(void){
 display.setTextSize(1);
  display.setTextColor(WHITE);
  display.setCursor(40,0);
 
  display.println("external drive");
  display.println("(pin 1) Delay:");

  if (cur_button_pressed == 3) 
  {
    external_delay++;
  }

  if (cur_button_pressed == 2) 
  {
    if (external_delay > 0)
    {
    external_delay--;
    }
  }

  if (cur_button_pressed == 4) 
  {
    external_delay = 0;
  }

  display.setTextSize(2);
  display.setCursor(0,17); 
  display.print(external_delay);
  display.println("ms");
  }


void update_accel(void){
  //Compute the appropriate acceleration to match frequency
  stepper.setAcceleration((int) (frequency/1000.0) *(frequency/1000.0) * 45.416195569154794);
  
}
void flip(){
  //flip shutter position
  if (shutter_cur_state == 0){
  stepper.moveTo(0);}
  else if (shutter_cur_state == 1){
  stepper.moveTo(closed_pos);}
  
  while (stepper.distanceToGo() != 0)
    {stepper.run();}
  
  shutter_cur_state = !shutter_cur_state;  
   
  }

 
